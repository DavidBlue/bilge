---
author: "Nathaniel"
handle: "@nathanieloffer"
source: "https://twitter.com/nathanieloffer/status/1459193349722705927"
likes: 1
retweets: 0
replies: 1
---
![nathanieloffer](media/nathanieloffer-150868226.jpg)
Nathaniel ([@nathanieloffer](https://twitter.com/nathanieloffer))



If you tweet saying you want to build better connections with people on twitter and then only allow people you've mentioned i.e. no one to reply I think you're missing the point.

---

![harrypotter1994](media/harrypotter1994-29424561.jpg)
Andrew ([@harrypotter1994](https://twitter.com/harrypotter1994))



[@nathanieloffer](https://twitter.com/nathanieloffer) Would’ve been funnier if you limited who could reply to this post.

---

![nathanieloffer](media/nathanieloffer-150868226.jpg)
Nathaniel ([@nathanieloffer](https://twitter.com/nathanieloffer))



[@harrypotter1994](https://twitter.com/harrypotter1994) I considered it