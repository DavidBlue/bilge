---
author: "1Password"
handle: "@1Password"
source: "https://twitter.com/1Password/status/1460609074857488390"
likes: 223
retweets: 56
replies: 31
---
![1Password](media/1Password-793926.jpg)
1Password ([@1Password](https://twitter.com/1Password))

![1Password Twitter Video](1Password8TwitterVideo.mp4)

1Password 8 for Windows is here! 



😍 All-new design

🌗 Dark mode

👩‍💻 Productivity boosters

🔐 Top-notch security, and so much more



Get started: [blog.1password.com/1password-8-fo…](https://blog.1password.com/1password-8-for-windows-is-here/?utm_medium=social&utm_source=twitter&utm_campaign=opw8ga&utm_ref=social) [pic.twitter.com/MKZVmhMB5C](https://twitter.com/1Password/status/1460609074857488390/video/1)