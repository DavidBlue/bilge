---
author: "Federico Viticci"
handle: "@viticci"
source: "https://twitter.com/viticci/status/1460602534171664386"
likes: 296
retweets: 48
replies: 29
---
![viticci](media/viticci-20904050.jpg)
Federico Viticci ([@viticci](https://twitter.com/viticci))



About two months ago, I switched back to [@tweetbot](https://twitter.com/tweetbot) as my primary Twitter client. 



If you haven’t tried Tweetbot in a while, it supports more Twitter features than ever thanks to the new API, and it’s frequently updated with modern iOS/iPadOS integrations. [macstories.net/ios/tweetbot-6…](https://www.macstories.net/ios/tweetbot-6-6-gets-support-for-creating-polls-limiting-who-can-reply-to-tweets/) [pic.twitter.com/PqjoDvDKqS](https://twitter.com/viticci/status/1460602534171664386/photo/1)

![3_1460602524487098369](media/3_1460602524487098369.jpg)

---

![ImGonzaloCampos](media/ImGonzaloCampos-94765340.jpg)
Gonzalo Campos ([@ImGonzaloCampos](https://twitter.com/ImGonzaloCampos))



[@viticci](https://twitter.com/viticci) [@tapbots](https://twitter.com/tapbots) [[@tweetbot](https://twitter.com/tweetbot)](https://twitter.com/tweetbot) What’s a more likely to happen… [[@Twitter](https://twitter.com/Twitter)](https://twitter.com/Twitter) buying @tweetbot and making it it’s official app with all the features enabled? Or @Twitter opening up it’s API to allow all its features to work on 3rd Party apps? 🤔

---

![Icynectarine](media/Icynectarine-710570148789424128.jpg)
subzeroplum ([@Icynectarine](https://twitter.com/Icynectarine))



[@ImGonzaloCampos](https://twitter.com/ImGonzaloCampos) [@viticci](https://twitter.com/viticci) [@tapbots](https://twitter.com/tapbots) [@tweetbot](https://twitter.com/tweetbot) [@Twitter](https://twitter.com/Twitter) Why would they do that? That’s literally how the first party twitter app came about, except that it was Tweetie they bough, at the time I switched to Tweetbot because they ruined tweetie.

---

![Icynectarine](media/Icynectarine-710570148789424128.jpg)
subzeroplum ([@Icynectarine](https://twitter.com/Icynectarine))



[@ImGonzaloCampos](https://twitter.com/ImGonzaloCampos) [@viticci](https://twitter.com/viticci) [@tapbots](https://twitter.com/tapbots) [@tweetbot](https://twitter.com/tweetbot) [@Twitter](https://twitter.com/Twitter) But then they changed the API and made apps like Tweetbot less and less useful over time. That combined with using Android phones for a while means I no longer use Tweetbot and won’t be going back because it requires a subscription and still isn’t able to replicate the full…

---

![Icynectarine](media/Icynectarine-710570148789424128.jpg)
subzeroplum ([@Icynectarine](https://twitter.com/Icynectarine))



[@ImGonzaloCampos](https://twitter.com/ImGonzaloCampos) [@viticci](https://twitter.com/viticci) [@tapbots](https://twitter.com/tapbots) [@tweetbot](https://twitter.com/tweetbot) [@Twitter](https://twitter.com/Twitter) …twitter experience.