---
author: "jack⚡️"
handle: "@jack"
source: "https://twitter.com/jack/status/1460975450763579395"
likes: 1293
retweets: 254
replies: 149
---
![jack](media/jack-12.jpg)
jack⚡️ ([@jack](https://twitter.com/jack))



Proud of our first major update to TIDAL!



We now have a FREE tier, with our entire catalog. Try it today…



And a first for artists: direct artist payouts and fan centered royalties! [twitter.com/tidal/status/1…](https://twitter.com/tidal/status/1460972083727503373)




> ![TIDAL](media/TIDAL-2679055230.jpg)
> TIDAL ([@TIDAL](https://twitter.com/TIDAL))
> 


> We're working to create a place that is better for artists and their fans. Because we love music. And the artists who create it. And the fans who crave it.



Today we're sharing progress on some new stuff: TIDAL Free, $10 HiFi, Direct Artist Payouts, and Fan-Centered Royalties. [twitter.com/jack/status/13…](https://twitter.com/jack/status/1367460909426176003)
> 



> ![jack](media/jack-12.jpg)
> jack⚡️ ([@jack](https://twitter.com/jack))
> 


> It comes down to a simple idea: finding new ways for artists to support their work. New ideas are found at the intersections, and we believe there’s a compelling one between music and the economy. Making the economy work for artists is similar to what Square has done for sellers.