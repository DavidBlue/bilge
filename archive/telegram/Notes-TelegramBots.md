# Notable Telegram Bots

- [GDPR Bot](https://t.me/gdprbot)
- [@scdlbot](https://t.me/scdlbot)
- [@TheFeedReaderBot](https://t.me/TheFeedReaderBot)
- [@TELEGRAPH](https://t.me/TELEGRAPH)
- [@Manybot](https://t.me/Manybot)