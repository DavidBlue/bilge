# Notes-Telegram

## General
* [x] **Telegram and Discord are the same age**
* [ ] [**Isiah’s random encounter**](https://twitter.com/ammnontet/status/1449594872139186181)
* [ ] Telegram’s cross-platform, resource-frugal clients and phone number-based accounts/auth literally make it more **class mobile** than Discord.
* [ ] What in fuck is this website… “[Telegram for iOS - NONE](http://tsfkb.wikidot.com/apps:ios)”
* [ ] My [personal Telegram satellite GitHub Repository](https://github.com/extratone/t)
* [x] [Send to Telegram Drafts Action](https://actions.getdrafts.com/a/18E)
* [x] [r/columbiamo - Local Telegram Group Chat for Columbia](https://reddit.com/r/columbiamo/comments/c4na0v/local_telegram_group_chat_for_columbia/)
* [x] Relative link to messages that don't involve complex scripting.
* [x] Unlimited file upload limit - **2GB PER FILE LIMIT**.
* [x] Cross-platform as all fuck.
* [ ] Every programming/scripting language ever has at least a few (probably unfinished and abandoned) projects in the wild building support for Telegram APIs.
* [x] Drafts as a safe dependable space to compose text does not have a windows equivalent, but Telegram can sort of function in that regard, largely because it continuously saves what one types in the text field. 
* [ ] Element … or Matrix… or whatever the actual protocol is called… is nothing but a great way to waste your time and the planet’s energy in obfuscating willy nilly. 
* [ ] Perhaps I should have accepted long ago that my particular priorities are *not* as resonant with other users as I would like to believe.
* [ ] Notification management in Telegram as well considered as I’ve ever experienced, which makes a lot of sense now that I am a long time user.
* [x] When I lost all my shit in Portland, much of what was saved was thanks to Telegram.
* [ ] The particular features that define Telegram’s competence are not immediately obvious, for the most part. Being able to quickly change the recipient of a forwarded message, for instance,
* [ ] Telegram is notably lacking in translation support considering it's such an international space.
* [ ] Microsoft Teams needs to be considered as an alternative since it's being so heavily emphasized for *personal use* in Windows 11.
* [x] The **cost** of Telegram vs. Discord Nitro, now.
* [ ] [[SI on Discord Regarding Objective-C and Swift]]
* [ ] "Last seen within a week will be kicked."
* [ ] As far as I’m concerned/as far as I live, now, Telegram is a required utility for using iOS.
* [ ] The ever-greater value of the ability to [**send silent messages**](https://telegram.org/blog/channels-2-0#silent-messages).
<<<<<<< Updated upstream
* YouTube's pre-sharesheet sharesheet pushes to Telegram even faster than Telegram's.
* [Anon ✪ in Telegram Desktop Talk](https://t.me/TelegramDesktopTalk/66912)
=======
* [ ] `t.me/s`
>>>>>>> Stashed changes


## References
- [ ] [**Telegram Privacy Policy**](https://telegram.org/privacy)
- [ ] [**Terms of Service**](https://telegram.org/tos)
- [ ] [Creating Desktop Themes – Telegraph](https://telegra.ph/Create-Telegram-Theme-01-12)
- [ ] [Drafts Directory: Send to Telegram](https://actions.getdrafts.com/a/18E)
- [ ] [Telegram | The Eastern Border](https://shows.acast.com/theeasternborder/episodes/telegram)
- [ ] [How-to: Markdown/Formatting Text in Telegram](https://telegra.ph/markdown-07-07)
- [ ] [Keyboard Shortcuts · telegramdesktop/tdesktop Wiki](https://github.com/telegramdesktop/tdesktop/wiki/Keyboard-Shortcuts)
- [ ] [Telegram VS Whatsapp | Hacker Noon](https://hackernoon.com/telegram-vs-whatsapp-25bf6f75c70f)
- [ ] [Revelations in Web Starvation](https://bilge.world/bad-connection-insights)
- [ ] [Nearby Search in Telegram Joke Tweet](https://twitter.com/neoyokel/status/1442188519309406212)
- [ ] [Telegram tops 1 billion downloads](https://techcrunch.com/2021/08/30/telegram-tops-1-billion-downloads/)
- [ ] [Telegram, nearing 500 million users, to begin monetizing the app – TechCrunch](https://techcrunch.com/2020/12/23/telegram-to-launch-an-ad-platform-as-it-approaches-500-million-users/)
- [ ] “[Chat History](https://reallifemag.com/chat-history/)” - *Real Life*
- [ ] [Why you shouldn't be using Telegram](https://tube.tchncs.de/w/2d958ef9-1be4-477c-bc13-852ec6391487)
- [x] [No, Don’t Quit WhatsApp To Use Telegram Instead—Here’s Why](https://www.forbes.com/sites/zakdoffman/2021/02/13/why-you-should-stop-using-telegram-instead-of-whatsapp-use-signal-or-apple-imessage) | *Forbes*
- [ ] [Telegram, Signal Are No Match for WhatsApp and Facebook's Network Effects](https://www.bloomberg.com/opinion/articles/2021-09-02/telegram-signal-are-no-match-for-whatsapp-and-facebook-s-network-effects) | *Bloomberg*
- [ ] [Telegram - Review 2021 - PCMag UK](https://uk.pcmag.com/iphone-apps/76144/telegram-messenger-for-iphone-review)
- [ ] [Mandatory WhatsApp Privacy Policy Update Allows User Data to be Shared With Facebook - MacRumors](https://www.macrumors.com/2021/01/06/whatsapp-privacy-policy-data-sharing-facebook/)
- [ ] [WhatsApp clarifies privacy practices after surge in Signal and Telegram users - The Verge](https://www.theverge.com/2021/1/12/22226792/whatsapp-privacy-policy-response-signal-telegram-controversy-clarification)
- [ ] [The "P" in Telegram stands for Privacy ~ inputzero](https://www.inputzero.io/2020/12/telegram-privacy-fails-again.html)
- [ ] [What’s Next for Communities at Discord](https://discord.com/blog/whats-next-for-communities-at-discord)
- [ ] [An Update on Our Business](https://discord.com/blog/an-update-on-our-business)
- [ ] [Happy Blurpthday to Discord, a Place for Everything You Can Imagine | by Nelly | Discord Blog](https://blog.discord.com/happy-blurpthday-to-discord-a-place-for-everything-you-can-imagine-fc99ee0a77c0)
- [ ] [How Discord (somewhat accidentally) invented the future of the internet - Protocol — The people, power and politics of tech](https://www.protocol.com/discord)
- [ ] [What Is Everybody Doing on Discord? - WSJ](https://www.wsj.com/articles/a-social-network-without-ads-discord-defies-convention-11615199401)
- [ ] [Discord Was Once The Alt-Right’s Favorite Chat App. Now It’s Gone Mainstream And Scored A New $3.5 Billion Valuation](https://www.forbes.com/sites/abrambrown/2020/06/30/discord-was-once-the-alt-rights-favorite-chat-app-now-its-gone-mainstream-and-scored-a-new-35-billion-valuation) | *Forbes*
- [ ] [Location-Based Chats, Adding Contacts Without Phone Numbers and More](https://telegram.org/blog/contacts-local-groups)
- [x] [React Native Newsletter](https://web.archive.org/web/20151207004934/http://brentvatne.ca/react-native-newsletter/07-09-2015.html) (Discord's iOS app originally launched in React Native.)
- [x] [Why Discord is Sticking with React Native | by Fanghao (Robin) Chen](https://blog.discord.com/why-discord-is-sticking-with-react-native-ccc34be0d427) | Discord Blog
- [ ] [Clearing Cache and Reordering Stickers](https://telegram.org/blog/cache-and-stickers) | Telegram Blog
- [x] [Voice Chats 2.0: Channels, Millions of Listeners, Recorded Chats, Admin Tools](https://telegram.org/blog/voice-chats-on-steroids) | Telegram Blog
- [x] [Live Streams, Flexible Forwarding, Jump to Next Channel, Trending Stickers and More](https://telegram.org/blog/live-streams-forwarding-next-channel) | Telegram Blog
- [x] [The Dawn of Video Has Come](https://blog.discord.com/5-10-2017-change-log-80f10c621c64) | Discord Blog
- [ ] "[Discord Screen Sharing and Video Chat](https://youtu.be/mMloc55o1jc)" - `https://www.youtube.com/watch?v=mMloc55o1jc`
- [x] [Discord Server Boost: How much does it cost to boost a Discord Server?](https://clutchpoints.com/discord-server-boost-cost/)
> A single boost costs $4.99 a month, and any member of the Discord Server can contribute. If you’re a Discord Nitro subscriber, you get a 30% discount on all Discord Server Boost purchases. Recently, Discord introduced lower thresholds for upgrading Discord Server Levels. Before, it takes 15 server boosts to achieve Level 2, and 30 server boosts to reach Level 3. But in a recent update, it will now only take 7 server boosts to reach Level 2, and 14 boosts to reach Level 3. That’d be a total of $34.93 for Level 2 and $69.86 for Level 3. That’s $24.45 for Level 2 and $48.90 for Level 3 for Nitro subscribers.

* [ ] [Chat Export Tool, Better Notifications and More](https://telegram.org/blog/export-and-more) | Telegram Blog
* [x] [Live Locations, Media Player and Languages](https://telegram.org/blog/live-locations) | Telegram Blog
* [x] [The man who wants to out-Uber Uber | CBC Radio](https://www.cbc.ca/radio/spark/346-biometrics-audio-intelligence-and-more-1.3987746/the-man-who-wants-to-out-uber-uber-1.3987987)
  * [x] [The man who wants to out-Uber Uber | CBC.ca](https://www.cbc.ca/player/play/879770691848)
  * [x] [LibreTaxi all orders – Telegram](https://t.me/s/libretaxi_all)
<audio controls>
  <source src="https://github.com/extratone/bilge/raw/main/audio/The%20man%20who%20wants%20to%20out-Uber%20Uber-CBC.mp3">
</audio>
- [ ] [Telegram Desktop reaches version 1.0 – and it's BEAUTIFUL](https://telegram.org/blog/desktop-1-0)
- [ ] [Telegram's Pavel Durov: Podcast 256](https://www.mixcloud.com/wiredUK/telegrams-pavel-durov-podcast-256/)
- [ ] [Telegram's Pavel Durov: Podcast 256](https://www.wired.co.uk/article/episode-256)
- [ ] [A Guide to Good Templates](https://instantview.telegram.org/checklist) | Telegram Blog



## Themes
- [Tweetbot 6 Pro](https://t.me/addtheme/Tweetbot6Pro)
- [Water Drops Space](https://t.me/addtheme/waterdropsspace)
- [SYSTEM COLORS Themes for iOS-Bound Telegram – Telegraph](https://telegra.ph/SYSTEM-COLORS-Themes-for-iOS-Bound-Telegram-09-27)
- [SYSTEM BLUE](https://t.me/addtheme/systemblue)
- [SYSTEM PINK](https://t.me/addtheme/systempink)
- [SYSTEM PURPLE](https://t.me/addtheme/systempurple)
- [SYSTEM RED](https://t.me/addtheme/systemred)
- [SYSTEM GREEN](https://t.me/addtheme/systemgreen)
- [SYSTEM ORANGE](https://t.me/addtheme/systemorange)

## Channels
* [Telegram iOS Talk](https://t.me/TelegramiOStalk)
* [It's FOSS](https://t.me/itsfoss_official)
* [Mastodon Club](https://t.me/mastodonclub)
* [PINE64 General Chat](https://t.me/mtrx_pine64)
* [LibreTaxi all orders](https://t.me/libretaxi_all)
* [iOS Themes Group](https://t.me/ThemeCreators)
* [Telegram Windows Phone Talk](https://t.me/TelegramWPtalk)

## Images

- [ ] `![Hungo](https://i.snap.as/35ChT1z2.jpeg)`
- [ ] `![SYSTEM COLORS](https://i.snap.as/W0JHUEKM.png)`
- [ ] `![SYSTEM COLORS Horizontal](https://i.snap.as/Y3fNeQP6.png)`
- [x] `![White Sapphire](https://i.snap.as/aNADRjvK.png)`
- [x] `![Telegram Green](https://i.snap.as/e2g09wl3.png)`
- [x] `![Drake Telegram Joke](https://i.snap.as/2YhRCO0I.jpeg)`
- [ ] `![Quartered Telegram](https://i.snap.as/387ADVNS.png)`
- [ ] `![Quarter Telegram](https://i.snap.as/wYNY5aVO.png)`
- [x] `![Patel Clouds Theme in the Chat Background Tool](https://i.snap.as/9Bxz27ZX.png)`
- [x] `![Local Visibility and Voice Notes Publishing in Telegram for iOS](https://i.snap.as/jfBVJqyw.png)`
- [x] `![Chat Export in Telegram Desktop](https://i.snap.as/TbPFRPnG.png)`
- [ ] `![iOS Old Red Theme](https://i.snap.as/cfzQk533.png)`
- [ ] `![Telegram for iOS Sharing and Notifications](https://i.snap.as/FltrCV6Z.png)`
- [x] `![Send to Telegram Drafts Action](https://i.snap.as/p3K96LiX.png)`
- [ ] `![Telegram in CarPlay](https://i.snap.as/0jm5rxyy.png)`
- [x] `![Security Considerations in Telegram for iOS](https://i.snap.as/Qim9gsZZ.png)`
- [x] `![Telegram Privacy - InputZero](https://i.snap.as/a0xgY5cJ.png)`
- [x] `![Telegram Desktop in Windows 11](https://i.snap.as/DFQzGGeZ.png)`
- [x] `![TG Colors](https://i.snap.as/r0WpvFQr.png)`
- [ ] `![Telegram Tile](https://i.snap.as/WQJGiVhY.png)`
- [ ] `![Commments Dot App Embed](https://i.snap.as/ur3rE2pw.png)`
- [x] `![Location Sharing in Telegram for iOS](https://i.snap.as/FRj5GGPN.png)`
- [x] `![Live Streams and Video Chats](https://i.snap.as/v1zhWmwN.png)`
- [x] `![Storage Management - Telegram for iOS](https://i.snap.as/BecGI6kg.png)`
- [x] `![Telegram Live Stream Meta](https://i.snap.as/oDQqSYHF.png)`
- [ ] `![Water Drops Space Theme - Telegram for iOS](https://i.snap.as/lorBpBov.png)`



## Anecdotes

* [ ] Static location sharing just sends a Lat-Long Google Maps URL: `https://maps.google.com/maps?q=38.934018,-92.388035&ll=38.934018,-92.388035&z=16`
* [ ] "[I don't really have anything to say about Telegram one way or the other. We ran it for a short time 5 years ago as an experiment and it didn't stick](https://discord.com/channels/836622115435184162/836622115880828961/898380609993449502)." - John Voorhees, *MacStories*
* [x] [New Telegram Group Chat thread in r/columbiamo](https://reddit.com/r/columbiamo/comments/q0843f/local_telegram_group_chat/)



## Dev

- [ ] [rahiel/telegram-send: Send messages and files over Telegram from the command-line.](https://github.com/rahiel/telegram-send#installation)
- [ ] [softwarehistorysociety/tg-archive: A tool for exporting Telegram group chats into static websites, preserving chat history like mailing list archives.](https://github.com/softwarehistorysociety/tg-archive)

## Media
* [Share Sheet animation](https://imgur.com/gallery/4laWgXj) - `https://imgur.com/gallery/4laWgXj`
* [Windows 11 Release Wake (Stream)](https://t.me/extratone/7054) - `https://t.me/extratone/7054`
* Apparently screen sharing to live video on iOS [is experiencing frame rate issues](https://t.me/TelegramiOStalk/104997).



***



<iframe id="reddit-embed" src="https://www.redditmedia.com/r/Telegram/comments/hv4rgk/i_love_this/?ref_source=embed&amp;ref=share&amp;embed=true" sandbox="allow-scripts allow-same-origin allow-popups" style="border: none;" height="auto" width="480" scrolling="no"></iframe>



<iframe style="border: 0; width: 100%; height: 450px;" allowfullscreen frameborder="0" src="https://raindrop.io/davidblue/telegram-20593542/embed/sort=-created&theme=auto"></iframe>