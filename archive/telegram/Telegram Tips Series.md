# Telegram Tips

## Sending a Full-Sized Image File from iOS
``![[SendFullSizedImage.mp4]]``

<video controls>
  <source src="https://user-images.githubusercontent.com/43663476/138945397-38e5ce91-5172-4b0a-bd45-467e0e02cf27.MP4">
</video>

The process to send the highest possible quality version of an image file to Telegram is actually much more specific than I thought - notably, I have operated for years under the false assumption that `Attachment → File → Photo or Video`  was sending "full quality" photo and video files.

