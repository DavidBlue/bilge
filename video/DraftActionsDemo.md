# Demo: Drafts Actions for the Write.as API

- [YouTube URL](https://youtu.be/eu8R9xG5FNA)
- [HardLimit](https://video.hardlimit.com/w/6Q6HQiuiFvxkAmCE2Fis59)



## Description

A demonstration of two Drafts actions for posting to Writeas. 

Post to Writeas: https://directory.getdrafts.com/a/1zO 

Post to Writeas Blog: https://directory.getdrafts.com/a/1zO

## Embed

### YouTube

```html
<iframe width="560" height="315" src="https://www.youtube.com/embed/eu8R9xG5FNA?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```



### HardLimit

```html
<iframe title="Demo: Drafts Actions for the Write.as API" width="560" height="315" src="https://video.hardlimit.com/videos/embed/2f34eb8c-3be8-4bda-a057-248939b869d0?warningTitle=0&amp;peertubeLink=0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
```



## Tags

```html
david blue,technology,software,computer hardware,ios,smartphones,tech blog,tech vlog,tech,write.as,draftsapp,snapas,cms,api,integration,draftsaction
```